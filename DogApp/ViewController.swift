//
//  ViewController.swift
//  DogApp
//
//  Created by COTEMIG on 27/10/22.
//

import UIKit
import Alamofire
import Kingfisher
import SwiftUI

struct actor: Decodable {
    let name: String
    let actor: String
    let image: String
    
}
class ViewController: UIViewController, UITableViewDataSource {
    
    var actorList:[actor]?
        
        func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
            return actorList?.count ?? 0
        }
        
        func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
            let cell = tableView.dequeueReusableCell(withIdentifier: "CellDog", for: indexPath) as! CellDog
            let actor = actorList![indexPath.row]
            
            cell.Title.text = actor.name
            cell.Subtitle.text = actor.actor
            cell.Photo.kf.setImage(with: URL(string: actor.image))
            
            return cell
        }

    

    @IBOutlet weak var TableView: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        apifunction()
        TableView.dataSource = self

    }
    
    private func apifunction(){
            AF.request("https://hp-api.herokuapp.com/api/characters")
                .responseDecodable(of: [actor].self){
                response in
                    if let actor_list = response.value{
                        self.actorList = actor_list
                }
                    self.TableView.reloadData()
            }
}
}
